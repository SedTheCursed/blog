import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string = 'Blog';
  posts = [
    {
      title:"Mon premier post",
      content:"Sed gravida dolor a arcu finibus, vitae euismod magna hendrerit. Cras eu nulla sit amet ligula sollicitudin ullamcorper. Morbi eu felis at dolor interdum tincidunt. In at posuere turpis. Suspendisse et neque elit. Nam lobortis eleifend venenatis. Praesent eget euismod orci.",
      loveIts: 3,
      create_at: new Date(2018, 10, 6, 12, 0)
    },
    {
      title:"Mon second post",
      content:"Aliquam velit leo, ultricies ut dignissim nec, laoreet vel mi. Suspendisse consectetur a neque at efficitur. Donec gravida odio id mi vehicula, a ullamcorper nunc imperdiet. Duis vitae congue nulla, at tincidunt lorem. Praesent sodales tristique consectetur. Praesent mattis ac nisl id volutpat. Curabitur ac pulvinar nibh.",
      loveIts: -2,
      create_at: new Date(2018, 10, 6, 13, 45)
    },
    {
      title:"Un autre post",
      content:"Quisque sit amet congue sem. In semper ligula quis enim rutrum hendrerit. Fusce et lacus a velit elementum fermentum vel tincidunt purus. Sed fermentum aliquet est, non tincidunt felis elementum ac. Cras vitae neque pulvinar, pellentesque leo eget, mattis ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In vestibulum risus libero, at consectetur quam fermentum sed. Fusce commodo a libero id imperdiet. Sed nulla ligula, laoreet ut odio sit amet, lobortis faucibus orci. Curabitur non diam non ipsum rhoncus tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut luctus libero a metus fermentum, eu bibendum libero sagittis. Aliquam facilisis vitae tellus bibendum laoreet. Nunc posuere volutpat tellus, quis ultricies risus tincidunt ac. Suspendisse fringilla viverra mi, ut tempus sem condimentum sit amet. In hac habitasse platea dictumst.",
      loveIts: 0,
      create_at: new Date(2018, 10, 6, 16, 30)
    }
  ]
}
